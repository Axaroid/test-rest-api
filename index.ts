import express, { Request, Response } from "express";
const app = express();
const port = 3000;
import invoiceRouter from './Routes/invoiceRouter';
import accountRouter from './Routes/accountRouter';
import morgan from 'morgan';

app.use(express.json());
app.use(morgan('dev'));

app.get('/', (req:Request, res:Response) => res.send('Hello World!'));

app.use('/api/', invoiceRouter);
app.use('/api/', accountRouter);

app.listen(port, () => console.log(`Example app listening on port ${port}!`));


