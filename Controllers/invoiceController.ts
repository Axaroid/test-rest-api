import { Request, Response } from "express";
import { z } from "zod";
import { PrismaClient, Prisma } from "@prisma/client";

const prisma = new PrismaClient();

const invoiceSchmea = z.object({
    date: z.preprocess((arg) => {
        if(typeof arg === "string" || arg instanceof Date) {
            return new Date(arg);
        }
    }, z.date()) ,
    amount: z.number().min(0, { message: "Amount must be greater than 0" }),
    accountID: z.string().trim()
});

type Invoice = z.infer<typeof invoiceSchmea>;
type Invoices = z.infer<typeof invoiceSchmea>[];

async function createInvoice(invoice: Invoice){
    return await prisma.$transaction([
        prisma.invoice.create({
            data: {
                invoice_date: invoice.date,
                invoice_amount: invoice.amount,
                Account: {
                    connect: {
                        account_id: invoice.accountID
                    }
                }
            }
        }),
        prisma.account.update({
            where: {
                account_id: invoice.accountID
            },
            data: {
                amount: {
                    increment: invoice.amount
                }
            }
        })
    ]);
}

// TODO look for a better way to do this
async function createInvoices(invoices: Invoices){
    // prisma does not allow to use relations in createMany
    // so we have to create invoices one by one
    const transaction = [];
    for(const invoice of invoices) {
        transaction.push(
            prisma.invoice.create({
                data: {
                    invoice_date: invoice.date,
                    invoice_amount: invoice.amount,
                    Account: {
                        connect: {
                            account_id: invoice.accountID
                        }
                    }
                }
            }),
            prisma.account.update({
                where: {
                    account_id: invoice.accountID
                },
                data: {
                    amount: {
                        increment: invoice.amount
                    }
                }
            })
        );
    }
    return await prisma.$transaction(transaction);
}

// TODO Improve error handling
// TODO Improve performance. Currently, if one invoice fails, all invoices fail
export const invoiceController = {
    create: async (req: Request, res:Response) => {
        try {
            if(Array.isArray(req.body)) {
                const invoices = z.array(invoiceSchmea).parse(req.body);
                const result = await createInvoices(invoices);
                res.status(201).json(result);
            } else {
                const invoice = invoiceSchmea.parse(req.body);
                const result = await createInvoice(invoice);
                res.status(201).json(result);
            }
        } catch(err:any) {
            if(err instanceof z.ZodError) {
                res.status(400).json({ message: err.issues });
            } else if (err instanceof Prisma.PrismaClientKnownRequestError) {
                if(err.code === "P2002")
                    res.status(400).json({ message: "Invoice already exists" });
                else 
                    res.status(400).json({ message: "Database error" });
            } else {
                res.status(500).json({ message: err.message });
            }
        }
    },
    get: async (req: Request, res:Response) => {
        try {
            const invoices = await prisma.invoice.findMany({
                include: {
                    Account: true
                }
            });
            res.status(200).json(invoices);
        } catch(err:any) {
            res.status(500).json({ message: err.message });
        }
    },
    getByID: async (req: Request, res:Response) => {
        try {
            const invoice = await prisma.invoice.findUnique({
                where: {
                    invoice_id: (req.params.id).toString()
                },
                include: {
                    Account: true
                }
            });
            if(invoice)
                res.status(200).json(invoice);
            else
                res.status(404).json({ message: "Invoice not found" });
        } catch(err:any) {
            res.status(500).json({ message: err.message });
        }
    },
    deleteByID: async (req: Request, res:Response) => {
        try {
            const invoice = await prisma.invoice.findUnique({
                where: {
                    invoice_id: (req.params.id).toString()
                },
                include: {
                    Account: true
                }
            });
            if(invoice) {
                const result = await prisma.$transaction([
                    prisma.invoice.delete({
                        where: {
                            invoice_id: (req.params.id).toString()
                        }
                    }),
                    prisma.account.update({
                        where: {
                            account_id: invoice.Account.account_id
                        },
                        data: {
                            amount: {
                                decrement: invoice.invoice_amount
                            }
                        }
                    })
                ]);
                res.status(200).json(result);
            } else {
                res.status(404).json({ message: "Invoice not found" });
            }
        } catch(err:any) {
            res.status(500).json({ message: err.message });
        }
    },
    updateByID: async (req: Request, res:Response) => {
        // supports updating only the amount or the date or both
        try {
            const invoice = invoiceSchmea.partial().parse(req.body);
            if(!invoice.date && !invoice.amount) {
                res.status(400).json({ message: "Nothing to update" });
                return;
            }
            // typescript >:( 
            if(invoice.amount) {
                if(invoice.amount < 0) {
                    res.status(400).json({ message: "Amount must be greater than 0" });
                    return;
                }
            }
            const oldInvoice = await prisma.invoice.findUnique({
                where: {
                    invoice_id: (req.params.id).toString()
                },
                include: {
                    Account: true
                }
            });
            let updatedAmount:number = invoice.amount || 0;
            // decrement the old amount and increment the new amount if the amount is updated
            if(oldInvoice && invoice.amount && oldInvoice.invoice_amount !== invoice.amount) {
                updatedAmount = invoice.amount - oldInvoice.invoice_amount;
            }
            if(oldInvoice) {
                const result = await prisma.$transaction([
                    prisma.invoice.update({
                        where: {
                            invoice_id: (req.params.id).toString()
                        },
                        data: {
                            invoice_date: invoice.date || oldInvoice.invoice_date,
                            invoice_amount: invoice.amount || oldInvoice.invoice_amount
                        }
                    }),
                    prisma.account.update({
                        where: {
                            account_id: oldInvoice.Account.account_id
                        },
                        data: {
                            amount: {
                              increment: updatedAmount   
                            }
                        }
                    })
                ]);
                res.status(200).json(result);
            } else {
                res.status(404).json({ message: "Invoice not found" });
            }
        } catch(err:any) {
            if(err instanceof z.ZodError) {
                res.status(400).json({ message: err.issues });
            } else if (err instanceof Prisma.PrismaClientKnownRequestError) {
                if(err.code === "P2002")
                    res.status(400).json({ message: "Invoice already exists" });
                else 
                    res.status(400).json({ message: "Database error" });
            } else {
                res.status(500).json({ message: err.message });
            }
        }
    }
} 