import { Request, Response } from "express";
import { string, z } from "zod";
import { PrismaClient, Prisma } from "@prisma/client";

const prisma = new PrismaClient();

const accountSchema = z.object({
    name: z.string()
            .min(3, { message: "Name must be at least 3 characters long" })
            .max(255, { message: "Name must be at most 255 characters long" })
            .trim(),
    amount: z.number().min(0, { message: "Amount must be greater than 0" }),
});

type Account = z.infer<typeof accountSchema>;
type Accounts = z.infer<typeof accountSchema>[];

async function createAccount(account: Account){
    return await prisma.account.create({
        data: {
            account_name: account.name,
            amount: account.amount,
        }
    });
}

async function createAccounts(accounts: Accounts){
    return await prisma.account.createMany({
        data: accounts.map(account => ({
            account_name: account.name,
            amount: account.amount,
        })),
    });
}

// TODO Improve error handling
export const accountController = {
    create: async (req: Request, res:Response) => {
        try {
            if(Array.isArray(req.body)) {
                const accounts = z.array(accountSchema).parse(req.body);
                const result = await createAccounts(accounts);
                res.status(201).json(result);
            } else {
                const account = accountSchema.parse(req.body);
                const result = await createAccount(account);
                res.status(201).json(result);
            }
        } catch(err:any) {
            if(err instanceof z.ZodError) {
                res.status(400).json({ message: err.issues });
            } else if (err instanceof Prisma.PrismaClientKnownRequestError) {
                if(err.code === "P2002")
                    res.status(400).json({ message: "Account already exists" });
                else 
                    res.status(400).json({ message: "Database error" });
            } else {
                res.status(500).json({ message: err.message });
            }
        }
    },
    get: async (req: Request, res:Response) => {
        try {
            const result = await prisma.account.findMany();
            res.status(200).json(result);
        } catch(err:any) {
            res.status(500).json({ message: err.message });
        }
    },
    getById: async (req: Request, res:Response) => {
        try {
            const result = await prisma.account.findUnique({
                where: {
                    account_id: (req.params.id).toString(),
                }
            });
            res.status(200).json(result);
        } catch(err:any) {
            if(err instanceof Prisma.PrismaClientKnownRequestError) {
                res.status(400).json({ message: "Database error" });
            } else {
                res.status(500).json({ message: err.message });
            }
        }
    },
    deleteById: async (req: Request, res:Response) => {
        try {
            const result = await prisma.account.delete({
                where: {
                    account_id: (req.params.id).toString(),
                }
            });
            res.status(200).json(result);
        } catch (err:any) {
            if(err instanceof Prisma.PrismaClientKnownRequestError) {
                res.status(400).json({ message: "Database error" });
            } else {
                res.status(500).json({ message: err.message });
            }
        }
    },
    updateById: async (req: Request, res:Response) => {
        // supports updating only the name and amount or both at the same time
        try {
            const account = accountSchema.partial().parse(req.body);
            if(!account.name && !account.amount) {
                res.status(400).json({ message: "No data to update" });
                return;
            }
            const result = await prisma.account.update({
                where: {
                    account_id: (req.params.id).toString(),
                },
                data: {
                    account_name: account.name,
                    amount: account.amount || undefined,
                }
            });
            res.status(200).json(result);
        } catch(err:any) {
            if(err instanceof z.ZodError) {
                res.status(400).json({ message: err.issues });
            } else if (err instanceof Prisma.PrismaClientKnownRequestError) {
                res.status(400).json({ message: "Database error" });
            } else {
                res.status(500).json({ message: err.message });
            }
        }
    },
};