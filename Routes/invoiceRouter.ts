import express from 'express';
import { invoiceController } from '../Controllers/invoiceController';

const router = express.Router();

router.get('/invoice/:id', invoiceController.getByID);
router.post('/invoice',invoiceController.create);
router.get('/invoices', invoiceController.get);
router.put('/invoice/:id', invoiceController.updateByID);
router.delete('/invoice/:id', invoiceController.deleteByID);

export default router;