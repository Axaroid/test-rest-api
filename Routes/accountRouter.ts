import express from 'express';
import { accountController } from '../Controllers/accountController';

const router = express.Router();

router.get('/account/:id', accountController.getById);
router.post('/account', accountController.create);
router.get('/accounts',accountController.get);
router.put('/account/:id', accountController.updateById);
router.delete('/account/:id', accountController.deleteById);

export default router;